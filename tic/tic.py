
import datetime, time

from sanic import response
import time
import uuid

from graph import g

NOW = lambda : int(time.time()*1000)
NEW_UUID = lambda : str(uuid.uuid4())

# ------------------------------------------------------------------------
def log_event(member_uuid, msg):

    r = g.run("""
        MATCH (m:Member {uuid:{member_uuid}}) 
        CREATE (e:EventLog {msg: {msg}, ts: timestamp()})<-[:_EVENT]-(m)
        RETURN null""", {'member_uuid': member_uuid, 'msg': msg}).evaluate()
    return r


# ------------------------------------------------------------------------
def js_now():
    d = datetime.datetime.utcnow()
    return int(time.mktime(d.timetuple())) * 1000

# ------------------------------------------------------------------------
def ts_to_date(ts):
    """javascript timestamp to date"""
    if ts:
        dt = datetime.datetime.fromtimestamp(float(ts)/1000.0)
        return dt
    else:
        return None    

# ------------------------------------------------------------------------
def date_to_ts(date):
    """date to javascript timestamp"""
    if date:
        d = date.strip()
        try:
            # assume it is just a year
            if len(d)==4:
                t = time.mktime(datetime.datetime.strptime(date, "%Y").timetuple())
            else:
                t = time.mktime(datetime.datetime.strptime(date, "%m/%d/%Y").timetuple())
            return int(t)*1000
        except:
            pass

    return 0

# ------------------------------------------------------------------------   
def get_remote_addr(request):
    # get the remote addr for this request; only interested in the IP# part
    # not the socket; should Sanic store it in a list instead?

    ip = request.headers.get('X-Forwarded-For', None)
    if not ip:
        try:
            ip = request.ip[0]
        except:
            ip = None

    return ip


# ------------------------------------------------------------------------
def log(msg):
    print(js_now(), msg)

