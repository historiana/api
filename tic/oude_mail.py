

import smtplib
from email.mime.text import MIMEText
from email.mime.multipart import MIMEMultipart

from email.message import EmailMessage
from email.headerregistry import Address

from config import CONFIG

#===============================================================================
def send_mail(template, data):
    """send a HTML and Text email; uses the templates in emails/"""

    print(" in sendmail")

    templ = "{}/{}.html".format(CONFIG.SITE.get('mail_templates'), template)

    msg = EmailMessage()

    # Create a text/plain message
    #with open('emails/{}.txt'.format(template)) as fp:
    #    text = fp.read()
    #    for (k,v) in data.items():
    #        text = text.replace('{{{}}}'.format(k), v)

    #    msg.set_content(text)

    msg.set_content('de tekst only')

    # Create a text/html message; assumes current working dir is correctly set
    with open(templ) as fp:
        html = fp.read()
        for (k,v) in data.items():
            #print("process {} met {}".format(k,v))
            html = html.replace('{{{}}}'.format(k), v)

    msg['Subject'] = data['subject']
    msg['From'] =  Address("Team Historiana", "webmaster", "historiana.eu")
    msg['To'] = data['to']
    #msg['Bcc'] = 'messages@coloresearch.com'
    msg.add_alternative(html, subtype='html')

    # deliver the message to local mailserver
    s = smtplib.SMTP('smtp.ziggo.nl')
    s.send_message(msg)
    s.quit()



# import smtplib
# from email.message import EmailMessage
# from email.utils import make_msgid
# from email.headerregistry import Address

# msg = EmailMessage()

# with open('test.txt') as fp:
#     # Create a text/plain message
#     text = fp.read()
#     for (k,v) in {'first': 'Eerstenaam', 'last': 'Achternaam', 'link':'http://www.xs4all.nl', 'subject': 'onderwerp'}.items() :
#     	text = text.replace('{{{}}}'.format(k), v)

#     msg.set_content(text)

# with open('test.html') as fp:
#     # Create a text/html message
#     html = fp.read()
#     for (k,v) in {'first': 'Eerstenaam', 'last': 'Achternaam', 'link':'http://www.xs4all.nl', 'subject': 'onderwerp'}.items() :
#     	html = html.replace('{{{}}}'.format(k), v)


# msg['Subject'] = 'testje... Subject'
# msg['From'] =  Address("Datacente.rs webmaster", "webmaster", "datacente.rs")
# msg['To'] = 'paulj@webtic.nl'

# msg.add_alternative(html, subtype='html')

# s = smtplib.SMTP('smtp.ziggo.nl')
# s.send_message(msg)
# s.quit()