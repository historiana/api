import json
import datetime
import neotime


class HiJSONEncoder(json.JSONEncoder):
    def default(self, obj):
        if (
            isinstance(obj, datetime.datetime)
            or isinstance(obj, datetime.date)
            or isinstance(obj, neotime.DateTime)
        ):
            return obj.__str__()

        if isinstance(obj, complex):
            return [obj.real, obj.imag]

        # Let the base class default method raise the TypeError
        return json.JSONEncoder.default(self, obj)


def HiJSONdump(data):
    return json.dumps(data, cls=HiJSONEncoder)


if __name__ == "__main__":
    print("main")
    from py2neo import Graph

    g = Graph(user="neo4j", password="geen", database="historiana", debug=True)

    data = g.run("RETURN {created:datetime({timezone:'CET'}), name:'iets'}").evaluate()
    print(json.dumps(data, cls=HiJSONEncoder))
