from graph import g
from sanic import Blueprint, response
from tic import api_response, status, NEW_UUID, js_now
from slugify import slugify
import os
import hashlib

from config import Config

CONFIG = Config()

PARTNER = Blueprint("partners", url_prefix="/api/partners")

# ------------------------------------------------------------------------
@PARTNER.route("/toggle/visibility", methods=["POST"])
async def toggle_visibility(request):

    session = request.headers.get("X-Session")
    # print("****** ", session)

    session = request.json.get("session")
    partner = request.json.get("partner")

    if not any([session, partner]):
        return api_response(status.NO_DATA)


    s = g.run(
        """
        MATCH (s:Session {sessionId:$session})
        OPTIONAL MATCH (s)-[]-(m:Member)
        RETURN {
            valid:s.sessionValid,
            user: m
        }
    """,
        session=session,
    ).evaluate()

    user = s.get("user")

    if not user:
        return api_response(status.USER_NOTFOUND)

    if not user['is_admin']:
        return api_response(status.NOT_ALLOWED)


    # toggle the is_visible field on the partner
    # print("partner: ", partner)
    # print(f"XXXX: _{partner}_")

    p = g.run("""MATCH (p:Partner {uuid:$partner}) RETURN p""", partner=partner).evaluate()
    if 'is_visible' in p.keys():
        p['is_visible'] = not p['is_visible']
    else:
        p['is_visible'] = True

    g.push(p)


    return api_response(status.OK)


# ------------------------------------------------------------------------
@PARTNER.route("/uploadBatch", methods=["POST"])
async def upload_batch(request):
    print("********* UPLOAD EXCEL ************")
    session = request.headers.get("X-Session")
    process = request.headers.get("X-Process")
    print("s: ", session)
    print("p: ", process)

    if process != "ExcelBatch":
        return api_response(status.ERROR)

    if session:
        # lookup session and match user if possible
        s = g.run(
            """
            MATCH (s:Session {sessionId:$sess})
            OPTIONAL MATCH (s)-[]-(m:Member)
            RETURN {
                valid:s.sessionValid,
                user: m
            }
        """,
            sess=session,
        ).evaluate()

        user = s.get("user")

    if not user:
        return api_response(status.ERROR)

    print("PROCESS UPLOAD FOR ", user)

    for (order, k) in enumerate(request.files.keys()):
        file = request.files.get(k)
        digest = hashlib.sha256(file.body).hexdigest()
        (fname, ext) = os.path.splitext(file.name)
        upload_uuid = NEW_UUID()
        upload = {
            "filename": file.name,
            "extension": ext.lower(),
            "digest": digest,
            "type": file.type,
            "uuid": upload_uuid,
        }

        relative = ["ExcelBatch"]
        relative.append(upload["uuid"])
        path = "/".join([CONFIG.USER_UPLOADS,] + relative)
        destination = path + "/" + file.name
        upload["path"] = "/".join(relative + [file.name])
        upload["created"] = js_now()
        os.makedirs(path, exist_ok=True)
        try:
            os.makedirs(path, exist_ok=True)
            with open(destination, "wb") as dest:
                dest.write(file.body)
                dest.close()

            print("PROCESS EXCEL: ", path)
            print("NAME: ", file.name)
            print("FOR: ", user)
        except Exception as e:
            print("ERROR: ", e)
            return api_response(status.ERROR, str(e))

    return api_response(status.OK)


# ------------------------------------------------------------------------
@PARTNER.route("/get-members", methods=["POST"])
async def getlist(request):

    search = request.json.get("search")

    data = g.run(
        """
        MATCH (m:Member) WHERE m.email CONTAINS $search
        RETURN collect(m.email)
        """,
        search=search,
    ).evaluate()

    return api_response(status.OK, data)


# ------------------------------------------------------------------------
@PARTNER.route("/", methods=["GET"])
async def index(request):
    data = g.run(
        """
        MATCH (p:Partner)
        OPTIONAL MATCH (p)-[:LOGO]-(logo)
        OPTIONAL MATCH (p)-[:ICON]-(icon)
        OPTIONAL MATCH (p)-[:PARTNER_ADMIN]-(member)
        WITH DISTINCT p,
            collect({url: '/ua/'+logo.path, uuid: logo.uuid})[0] as logo,
            collect({url: '/ua/'+icon.path, uuid: icon.uuid})[0] as icon,
            collect(member.email)[0] as member
        ORDER BY lower(p.name)
        RETURN collect( DISTINCT p {
            .*,
            logo,
            icon,
            member
        })

    """
    ).evaluate()

    # data = g.run(
    #     """
    #     MATCH (p:Partner)
    #     WITH p
    #     ORDER BY p.name
    #     RETURN collect(p)
    # """
    # ).evaluate()

    return api_response(status.OK, data)


# ------------------------------------------------------------------------
@PARTNER.route("/delete", methods=["POST"])
async def delete(request):
    uuid = request.json.get("uuid")
    g.run(
        """
            MATCH (p:Partner {uuid:$uuid})
            DETACH DELETE p
        """,
        uuid=uuid,
    )
    return api_response(status.OK)


# ------------------------------------------------------------------------


@PARTNER.route("/add", methods=["POST"])
@PARTNER.route("/edit", methods=["POST"])
async def partner_add_edit(request):
    print(50 * "=")
    print("add/edit partner!")
    # split current url, last non-empty element is either edit or add
    mode = [e for e in request.path.split("/") if e].pop()

    record = request.json
    print("Record: ", record)
    print("MODE: ", mode)

    record["slug"] = slugify(record["name"])

    if mode == "add":
        partner = g.run(
            """
                CREATE (p:Partner)
                SET p.name = $name,
                p.slug = $slug,
                p.uuid = apoc.create.uuid(),
                p.logo = $logo['url'],
                p.icon = $icon['url'],
                p.introduction = $introduction
                RETURN p
        """,
            record,
        ).evaluate()

    elif mode == "edit":
        partner = g.run(
            """
                MATCH (p:Partner {uuid:$uuid})
                OPTIONAL MATCH (p)-[l:LOGO]-(a)
                OPTIONAL MATCH (p)-[i:ICON]-(a)
                DELETE l,i
                WITH p
                SET p.name = $name,
                p.slug = $slug,
                p.logo = $logo['url'],
                p.icon = $icon['url'],
                p.introduction = $introduction
                RETURN p
        """,
            record,
        ).evaluate()

    else:
        print("INVALID MODE")
        return api_response(status.NOT_FOUND)

    # connect logo to new partner
    val = g.run(
        """
            MATCH (p:Partner {uuid:$partner_uuid})
            MATCH (l {uuid:$logo_uuid})
            CREATE (p)-[x:LOGO]->(l)
            RETURN x
        """,
        partner_uuid=partner["uuid"],
        logo_uuid=request.json.get("logo")["uuid"] if request.json.get("logo") else "",
    ).evaluate()

    # connect icon to new partner
    g.run(
        """
            MATCH (p:Partner {uuid:$partner_uuid})
            MATCH (l {uuid:$icon_uuid})
            CREATE (p)-[x:ICON]->(l)
            RETURN x
        """,
        partner_uuid=partner["uuid"],
        icon_uuid=request.json.get("icon")["uuid"] if request.json.get("icon") else "",
    )

    # delete existing member
    g.run(
        """
            MATCH (p:Partner {uuid:$partner_uuid})
            MATCH (m:Member {email:$member})
            OPTIONAL MATCH (p)-[pa:PARTNER_ADMIN]-(m)
            DELETE pa
    """,
        partner_uuid=partner["uuid"],
        member=request.json.get("member"),
    ).evaluate()

    # connect member to this partner
    g.run(
        """
            MATCH (p:Partner {uuid:$partner_uuid})
            MATCH (m:Member {email:$member})
            OPTIONAL MATCH (p)-[pa:PARTNER_ADMIN]-(m)
            DELETE pa
            CREATE (p)<-[x:PARTNER_ADMIN]-(m)
            WITH m
            SET m.is_partner=TRUE
            RETURN m
        """,
        partner_uuid=partner["uuid"],
        member=request.json.get("member"),
    ).evaluate()

    return api_response(status.OK, partner)


# ------------------------------------------------------------------------
@PARTNER.route("/<slug>", methods=["GET"])
async def get_partner(request, slug):

    if not slug:
        return api_response(status.NO_DATA)

    """
    tricky query alert: .* normally returns props of first parameter
    but in order for the thumb to retrieve correctly we need to swap it for the 'item'
    NOT SURE WHY - Neo 3.5.12
    - alternative writing:

        WITH collect( col {
            .*,
            thumb: '/ua/'+a.path
            }) as collections
        RETURN collections

    see mycollections.py index method

    """
    data = g.run(
        """
        MATCH (p:Partner {slug:$slug})
        OPTIONAL match (p)-[]-(m:Member)-[]-(col:Collection {is_published:true})
        OPTIONAL MATCH (p)-[:LOGO]-(logo)
        OPTIONAL MATCH (p)-[:ICON]-(icon)
        OPTIONAL MATCH (col)-[:THUMB_FOR]-(thumb)
        WITH p, logo, icon {
            .*,
            logo: logo.url,
            icon: icon.url
        },

        thumb, col {
            .*,
            thumb_url: '/ua/'+thumb.path,
            csstype:'collection',
            type: 'Collection'
        } as item, m

        RETURN {
            partner: p,
            owner: m.uuid,
            collections: collect(distinct item)
        }
    """,
        slug=slug,
    ).evaluate()

    activities = g.run(
        """
        MATCH (p:Partner {slug:$slug})
        OPTIONAL match (p)-[]-(m:Member)-[]-(col:MyActivity {is_published:true})
        OPTIONAL MATCH (p)-[:LOGO]-(logo)
        OPTIONAL MATCH (p)-[:ICON]-(icon)
        OPTIONAL MATCH (col)-[:HAS_IMAGE]-(thumb)
        WITH p, logo, icon {
            .*,
            logo: logo.url,
            icon: icon.url
        },

        thumb, col {
            .*,
            thumb_url: '/ua/'+thumb.path,
            type: 'e-Learning Activity',
            csstype:'ela'


        } as item, m

        RETURN {
            partner: p,
            owner: m.uuid,
            activities: collect(distinct item)
        }
    """,
        slug=slug,
    ).evaluate()


    if data:
        data["activities"] = activities['activities']
        return api_response(status.OK, **data)

    elif activities:
        return api_response(status.OK, **activities)

    return api_response(status.NO_DATA)


# ------------------------------------------------------------------------
@PARTNER.route("/check_email", methods=["POST"])
async def check_new_partner(request):
    email = request.json.get("email")
    print("\n\n** CHKECK **: ", email)

    if email:
        print("GO")
        data = g.run(
            """MATCH (m:Member {email:$email}) RETURN m""", email=str(email),
        ).evaluate()
        print("DATA: ", data)

        if data:
            print("FOUND")
            return api_response(
                status.OK, uuid=data["uuid"], isMember=True, record=data
            )
        else:
            print("NOT FOUND")
            return api_response(status.OK, isMember=False)

    return api_response(status.ERROR)
