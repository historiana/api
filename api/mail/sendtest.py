# test the environment by sending a password reset message

import os
import jinja2 

import mysmtplib

#from sendmail import send_mail

from email.message import EmailMessage
from email.headerregistry import Address
from email.mime.text import MIMEText
from email.utils import make_msgid


template_folder = "/".join(os.getcwd().split('/')[:-2])+'/email-templates'
loader = jinja2.FileSystemLoader(searchpath=template_folder)
env = jinja2.Environment(loader=loader)
template = env.get_template('reset-password.html')

msg = EmailMessage()
msg_id = make_msgid()
logo_id = '42'

print("id: ", msg_id)

# Create a text/plain message
msg.set_content("dit is platte tekst")
# Create text/html message
msg.add_alternative(template.render(locals()), subtype='html')

# add the logo
with open(f"{template_folder}/email-logo.png", 'rb') as img:
	msg.get_payload()[1].add_related(img.read(), 'image', 'png', cid=logo_id)


msg['Subject'] = 'SSL is nu beschikbaar!'
msg['From'] = 'web@historiana.eu'
msg['To'] = 'paulj@xs4all.nl'

# Send the message via our own SMTP server.
s = mysmtplib.SMTP('mail.webtic.net', 25)
s.set_debuglevel(0)
s.connect('mail.webtic.net', 587)
s.starttls()
s.login('web@historiana.eu', 'IpxxqFqFsC5C')
status = s.send_message(msg)
s.quit()

print("STATUS: ", status)

from graph import g

#g.run("CREATE (l:Log) SET l.action='reset-password-mail-sent', l.to={to}, l.ts=timestamp(),l.msgId={mid}", to=msg['To'], mid=msg_id)




