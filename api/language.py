from graph import g
from sanic import Blueprint, response
from tic import api_response, status
from slugify import slugify

LANGUAGE = Blueprint("language", url_prefix="/api/language")

# ------------------------------------------------------------------------
@LANGUAGE.route("/", methods=["GET"])
async def getlist(request):

    data = g.run(
        """
        MATCH (l:Language)
        WITH l
        ORDER BY l.name ASC        
        WITH l,l.native+' ('+l.name+')' as label
        RETURN collect(l {
            .*,
            label:label
        }) 
        """
    ).evaluate()

    return api_response(status.OK, data)
