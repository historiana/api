

"""
MATCH (n {uuid:'63775506-4413-48c5-bba1-212fc4e2a5f4'})
        OPTIONAL MATCH (n)-[r]-(n1)
        WITH count(n1) as cnt,r,n,n1
        WITH n,n1,COLLECT(
        {	
        		count: cnt,
                rel: type(r),
                node: labels(n1)
                
        }) as relations
        WITH relations,n
        WITH n,COLLECT(
        
        {
            props: properties(n),
            labels: labels(n),
            related: relations
        }) as meta
        RETURN meta

"""


# props for Label
# match (n:Collection) with collect(keys(n)) as c return apoc.coll.toSet(apoc.coll.flatten(c))

models = {}

class CollectionItem:
	def __init__(self):
		self.name = 'CollectionItem'
		self.props = ["title", "url", "description", "filename", "order", "uuid"]
		self.meta = ["title", "url", "filename", "order", "uuid"]



class Collection:

	def __init__(self):
		self.name = 'Collection'
		self.props = [
			"name", 
			"subtitle", 
			"modified", 
			"slug", 
			"acknowledgements", 
			"description", 
			"pptx", 
			"uuid", 
			"ts", 
			"status"
		],

		self.meta = [
			"name",
			"slug", 
			"modified", 
			"uuid", 
			"ts", 
			"status"
		]



models['Collection'] = Collection()
models['CollectionItem'] = CollectionItem()
