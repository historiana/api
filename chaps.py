

from graph import g

slug = 'world-war-1'

c = g.run("""
    MATCH (m:Module {slug:{slug}}) 
    OPTIONAL MATCH (m)-[]-(cr:Copyright)
    OPTIONAL MATCH (m)-[:ICONIC_IMAGE]-(a:Asset)

    RETURN {
        module: m,
        copyright: cr,
        thumb: "/objects/"+a.uuid+"/"+a.thumb
    }

""", slug=slug).evaluate()


# the old ORM processing starts here...
# for compatibility we need to do it like this for now
# new stuff needs to be remodeled

entries = []
items = {}

# get the ModuleItem to start on
start = g.run("""
    MATCH (m:Module {slug:{slug}})-[:NEXT]-(mi:ModuleItem)-[:CURRENT]-(c:ContentItem)
    RETURN {
        next: mi,
        chapter: {
            name: c.title,
            slug: c.slug
        }
    }
""", slug=slug).evaluate()

# get all the next chapters via the NEXT relation on a ModuleItem
entries.append(start['chapter'])
next = start['next']


items[start['chapter']['slug']] = []

for iets in next.end_node().match('ITEMS'):
    print("NEXT: ", iets, iets.labels())
    print("IETSS: ", iets.start_node().labels())
    print("IETSE: ", iets.end_node().labels())

    dieper = iets.end_node()

    for bla in dieper.match('CURRENT'):
        print("bla: ", bla, bla.labels())
        print("x: ", bla.start_node()['title'])

        items[start['chapter']['slug']].append(bla.start_node()['title'])



while next:
    #print("next: ", next)
    done = True
    for module_item in next.match_outgoing(rel_type='NEXT'):
        done = False
        print("module_item: ", module_item, module_item.labels())
        nieuwe = module_item.end_node()
        print("nieuwenext: ", nieuwe, nieuwe.labels())
        for iets in module_item.end_node().match('CURRENT'):
            bla = iets.start_node()
            entries.append({'name': bla['title'], 'slug': bla['slug']})
            print("----> ", bla.labels(), bla['title'])
            current = bla['slug']
            items[current] = []

        print("")
        for iets in nieuwe.end_node().match('ITEMS'):
            print("current: ", current)
            print("NEXT: ", iets, iets.labels())
            print("IETSS: ", iets.start_node().labels())
            print("IETSE: ", iets.end_node().labels())

            dieper = iets.end_node()

            for bla in dieper.match('CURRENT'):
                print("bla: ", bla, bla.labels())
                print("x: ", bla.start_node()['title'])
                rec = dict(
                    title = bla.start_node()['title'],
                    slug = bla.start_node()['slug']
                )
                # items[current].append(bla.start_node())
                data = dict(item = rec)
                data['downloads'] = []
                data['learning_activities'] = []
                for foobar in bla.start_node().match('RELATED_DOWNLOADS'):
                    data['downloads'].append(foobar.start_node())

                for foobar in bla.start_node().match('ICONIC_IMAGE'):
                    data['icon'] = foobar.start_node()

                for foobar in bla.start_node().match('RELATED_LA'):
                    data['learning_activities'].append(foobar.end_node())

                items[current].append(data)


    next = False if done else nieuwe
            
# these are the main chapters for a Module
# sc['entries'] = entries


print("ENTRIES")
import pprint
pprint.pprint(items)