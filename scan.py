import os

from graph import g

for file in os.listdir("../historiana-thriving/dist"):
    if file.endswith(".html"):
        print(file)
        a = g.run("MERGE (t:_ticTemplate {file:'%s'}) RETURN t" % file) 

        print("a: ", a)
