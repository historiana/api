from graph import g
from tic import api_response, status


def index(request):
    print("collections index")
    collections = g.run(""" 
        MATCH (c:Collection) 
        OPTIONAL MATCH (c)-[:HAS_LICENSE]->(l:License) 
        WITH c,l
        ORDER BY c.ts DESC
        RETURN collect({
            ts: c.ts*1000,
            name: c.name,
            subtitle: c.subtitle,
            status: c.status,
            license: l.name,
            uuid: c.uuid,
            slug: c.slug
        }) 


    """).evaluate()

    return api_response(collections)

def delete(request):
    """delete Collection AND its CollectionItems"""
    collection = request.json.get('collection')

    # TODO: validate if delete is permitted
    user = request.json.get('user')

    g.run("""
        MATCH (c:Collection {uuid:{collection}}) 
        OPTIONAL MATCH (c)<-[:IN_COLLECTION]-(ci:CollectionItem)
        DETACH DELETE c,ci
    """, collection=collection)
    return api_response(status.OK)

def show(request):

    print("SHOW========")
    return api_response("ok")



def delete_thumb(request):
    """delete the thumb button"""
    g.run("""
        MATCH (c:Collection {uuid:{collection}})-[t:THUMB_FOR]-(x)
        DELETE t
    """, request.json)


    return api_response(status.OK)


