from flask import Blueprint, render_template, abort, request, redirect, jsonify, send_from_directory, url_for
from jinja2 import TemplateNotFound

import sys
import os
import os.path
import util
import requests
import urllib
import json
import time

from graph import g

class config:
   WSKEY = 'VBCZAGGDZI'
   DEBUG = True

class Context(object):
   pass

sas = Blueprint('sas', __name__) 

@sas.route('/admin')
def admin():

   return render_template('sas/APP-sourcecollection.html')

# ====================================================================
@sas.route('/assets/<path:path>')
def send_assets(path):
   n = os.path.dirname(os.path.realpath(__file__))
   prefix = "/".join(n.split('/')[:-1])+"/assets/"
   return send_from_directory(prefix, path)

# ====================================================================
@sas.route('/logo/<path:path>')
def send_logo(path):
   n = os.path.dirname(os.path.realpath(__file__))
   prefix = "/".join(n.split('/')[:-1])+"/uploads/"
   return send_from_directory(prefix, path)

# ====================================================================
@sas.route("/get_data_provider")
def get_data_provider():
   """get the data  for Add new Searchpartner > Europeana code 
   client site search now
   """
   search = request.args.get("s", None)
   c = "/tmp/historiana_get_data_provider.json"
   create = False

   # API stuk, geen nieuwe data proberen te maken
   # try:
   #     stamp = os.path.getmtime(c)
   #     now =  time.time()
   #     # 24 hours old?
   #     if now - stamp > 86400:
   #         create = True

   # except OSError as e:
   #     create = True

   if create:        

       url = "https://www.europeana.eu/api/v2/search.json"
       p = {
           'wskey':    config.WSKEY ,
           'query':    '*',
           'facet':    'DEFAULT',
           'profile':  'portal',
           'rows':     1        
       }

       r = requests.get(url, params=p)
       data = r.json()['facets']
       names = [ {'name':x['label']} for x in [ b['fields']  for b in data  if b['name']=='DATA_PROVIDER'][0]]


       resp = {
           'items': names,
       }

       # cache this data
       f = open(c,"w")
       f.write(json.dumps(resp))
       f.close()

       return json.dumps(resp)

   return open(c).read()



# ====================================================================
@sas.route('/partner/<slug>', methods=['GET','POST'])
def partner(slug):
   print "*** PARTNER ***"
   c = Context()
   # we either searched for something specific OR we are scrolling the partner page, hence browse '*'
   s = request.form.get('search_for','*')
   uuid = request.args.get("u", None)
   if not uuid:
       return abort(404)

   r = g.cypher.execute("MATCH (partner:SearchPartner {uuid:{uuid}})-[:HAS_LOGO]-(logo:NewUpload) RETURN partner,logo", {'uuid':uuid})
   c.partner = r.one.partner
   c.logo = r.one.logo

   # this is a local cached number of items per partner.
   # not needed anymore; we use the API instead.
   # r = g.cypher.execute("MATCH (c:EA_provider {name:{name}}) RETURN c.count LIMIT 1", {'name':c.partner['name']})
   # print "count: ", r.one
   # c.count = r.one

   #http://www.europeana.eu/api/v2/search.json?wskey=VBCZAGGDZI&query=*&facet=DEFAULT&profile=portal&qf=DATA_PROVIDER:%22Ghent%20University%20Library%22

   url = "https://europeana.eu/api/v2/search.json"

   p = {
       'wskey':    config.WSKEY ,
       'query':    s if s else '*',
       'facet':    'DEFAULT',
       'profile':  'rich',
       'rows':     12,
       'qf':       'DATA_PROVIDER:"%s"' % str(c.partner["name"]),
       'cursor':   '*',

   }

   r = requests.get(url, params=p)
   data = r.json()#['facets']
   for item in data['items']:

       try:
           item['object_description'] = ", ".join(item['edmConceptPrefLabelLangAware']['en'])
       except:
           try:
               item['object_desacripton'] = item['dctermsSpatial']
           except:
               pass


   return render_template("sas/partner.html", c=c, s=s, data=data, str=str)


# ====================================================================
@sas.route('/mySources')
def my_sources():
   return render_template("sas/APP-mysources.html")

# ====================================================================
@sas.route('/myUpload')
def upload():
   return render_template("sas/APP-upload.html")



# ====================================================================
@sas.route("/eu", methods=['POST'])
def eu():

   data = request.get_json()


   url = "https://europeana.eu/api/v2/search.json"
   p = {
       'wskey':    config.WSKEY ,
       'query':    '%s' % data['search'],
       'facet':    'DEFAULT',
       'profile':  'rich',
       'rows':     12,     
       'cursor':   data['cursor'],
   }

   for qf in data['facets']:
       #print "qf: ", qf
       (field, data) = qf.split(':')
       p['qf'] = '%s:"%s"' % (field, data)

   r = requests.get(url, params=p)
   data = r.json()#['facets']
   #print "r: ", data['totalResults']

   try:
       # remove items zonder edmPreview (thumbnail
       items = []
       for o,i in enumerate(data['items']):
           if not i.has_key('edmPreview'):
               i['edmPreview']=["http://placehold.it/240?text=No+image"]
               #items.append(i)
   except:
       # probably no items?
       pass

   cursor = data['nextCursor']
   html = render_template("sas/search_results_ajax.html", data=data)
   return json.dumps({'html': html, 'nextCursor': cursor})


@sas.route('/search', methods=['GET', 'POST'])
def search():

# Steven feedback:
# - UGC = Waarschijnlijk niet nodig: Zeker niet binnen de resultatenpagina van 1 search partner. Wellicht wel op
# - Language = Waarschijnlijk niet nodig:
# - Country = Meer onderzoek nodig. Zeker binnen de resultatenpagina van 1 search partner.
# - Colour = Zeker niet nodig.
# - Type = Zeker niet nodig (we laten alleen images zien).
# - Text_Fulltext = Zeker niet nodig (we laten alleen images zien).
# - Year = Waarschijnlijk handig maar geen eindelose lijst
# - Rights = Waarschijnlijk handig: Omdat een gebruiker dan onderscheid kan maken tussen de verschillende type rechten. Als commercieel gebruik niet mag, is dat handig om te weten.


   hides = [ e.upper() for e in [
       u'UGC',
       u'Language',
       u'Colour',
       u'Type',
       u'Text_Fulltext',
       u'Mime_Type',
       u'Sound_HQ',
       u'VIDEO_DURATION',
       u'Sound_Duration',
       u'COLOURPALETTE',
       u'IMAGE_COLOUR',
       u'MEDIA',
       u'Mime_Type',
       u'COLOURPALETTE',
       u'IMAGE_COLOUR',

   ]]


   print "** SEARCH"
   c = Context()

   if request.method=='GET':
       c.data = g.cypher.execute("MATCH (partner:SearchPartner)-[:HAS_LOGO]-(logo:NewUpload) RETURN partner,logo ORDER BY partner.name")
       return render_template("sas/search.html",c=c, slugify=util.slugify)

   import httplib as http_client
   http_client.HTTPConnection.debuglevel = 1
   import logging
   logging.basicConfig() 
   logging.getLogger().setLevel(logging.DEBUG)
   requests_log = logging.getLogger("requests.packages.urllib3")
   requests_log.setLevel(logging.DEBUG)
   requests_log.propagate = True


   # we have form data
   s = request.form.get('search_for','*')
   print "s: ", s
   url = "https://www.europeana.eu/api/v2/search.json"
   p = {
       'wskey':    config.WSKEY ,
       'query':    '%s' % s,
       'facet':    'DEFAULT',
       #'profile':  'portal',
       'profile':  'rich',
       'rows':     12,
       'media':    True,               # grotere media beschikbaar
       'cursor':   '*',
   }

   print "SSS"
   r = requests.get(url, params=p)

   print "url: ", r.url

   data = r.json()#['facets']

   # instead of breaking our interface we stop
   if 'error' in data.keys():
       return "We are sorry; the Europeana API returned an error: %s" % data['error']



   print "CCCC: ", data['nextCursor']
   #print "r: ", data['totalResults']

   #import pprint
   #pprint.pprint(data)

   # remove items zonder edmPreview (thumbnail
   items = []
   for o,i in enumerate(data['items']):
       if not i.has_key('edmPreview'):
           i['edmPreview']=["http://placehold.it/240?text=No+image"]
           #items.append(i)

   #data['items'] = items
   #data['totalResults'] = len(items)


   # profile=portal gives facets; that is broken now
   # for unknown reasons May 16 2016
   # verwijder alle FACETs die we niet in de interface willen
   deletes = []

   if 'facets' in data.keys():
       for (i,d) in enumerate(data['facets']):
           if d['name'] in hides:
               deletes.append(i)

       # achteraan beginnen anders kloppen de verzamelde offsets niet
       deletes.reverse()
       for d in deletes:
           del data['facets'][d]



   return render_template("sas/search_results.html", c=c, s=s, data=data, str=str)

# ====================================================================
@sas.route('/collection/<slug>')
def collection(slug):

   col = g.cypher.execute("""
         MATCH (col:Collection {slug:{slug}})-[:IN_COLLECTION]-(entry:CollectionItem) 
         OPTIONAL MATCH (entry)-[:HAS_LICENSE]->(l:License)          
         OPTIONAL MATCH (col)-[:THUMB_FOR]-(thumb:NewUpload) 
         OPTIONAL MATCH (ml:License)<-[:HAS_LICENSE]-(col)
         RETURN col,entry,thumb,l,ml order by entry.order""", {'slug':slug})

   #col = g.cypher.execute("MATCH p=(col:Collection {slug:{slug}}) RETURN p""", {'slug':slug})
   print col

   return render_template("sas/show_sourcecollection.html", collection=col)


# ====================================================================
@sas.route('/')
def home():

   #collections = g.cypher.execute("MATCH (c:Collection) RETURN c")
   collections = g.cypher.execute("MATCH (n:Collection) OPTIONAL MATCH (n)-[:THUMB_FOR]-(thumb:NewUpload) return n, thumb")

   #for c in collections:
   #    print c['n']['name'], c['thumb']

   return render_template("sas/APP-home.html", collections=collections)