
System Documention
====================


--------------------------------------------------------
frontend: 

/#/teaching-learning
import TL from '@/pages/TeachingLearning'
API.post('/get-teaching-learning', user)
in api.py / get_teaching_learning

/api/get-teaching-learning


--------------------------------------------------------
Backend:

/#/admin/LearningActivity
LearningActivityEditor.vue

index:  
	url: 	/api/la
	code: 	api.py:get_learning_activity

edit: 
	url:	/api/la/<uuid>
	code: 	api.py:edit_learning_activity

save:  /admin/la/<uuid> POST
	code: admin/learningactivities.py:saveLA