#!/usr/bin/python3
from sanic import Sanic
from sanic.response import json, redirect, text
from sanic.exceptions import abort

import sanic
import datetime
from graph import g

import tic

app = Sanic(__name__)

"""
If there a no Redirect nodes with a num: property the code fails
Do a CREATE (r:Redirect) SET r.num=0 to fix this.
"""

# ------------------------------------------------------------------------
@app.middleware('request')
async def set_remote_addr(request):
    """create a remote_addr attribute on the current request for logging purposes"""
    if 'x-forwarded-for' in request.headers:
        request.headers['remote_addr'] = request.headers['x-forwarded-for']
    else:
        request.headers['remote_addr'] = request.ip[0]

# ------------------------------------------------------------------------
def log_request(request, code, status, to=''):
    with open("/var/log/hi.st.log", "a") as log:
        log.write('{} - {} - {} status:{} to:{}\n'.format(
            datetime.datetime.now(),
            request.headers['remote_addr'],
            code,
            status,
            to
        ))
        log.close()

# ------------------------------------------------------------------------
@app.route('/<code>',  methods=['GET'])
async def serve(request, code=None):
    """check Redirect nodes for a matching redirect"""

    try:
        r = g.run("""
            MATCH (r)
            WHERE r.code={code}
            OR r.num={num}
            RETURN collect(r) AS r
        """, {'code': code, 'num': tic.base66.decode(code)}).evaluate()[0]

        if r:
            r['last_redirect'] = tic.js_now()
            r['redirects'] = r['redirects'] + 1 if r['redirects'] else 1
            r.push()
            log_request(request, code, 302, r['to'] )

        return redirect(r['to'])

    except:
        pass

    log_request(request, code, 404)
    return text('Nothing found',404)



# ------------------------------------------------------------------------
@app.route('/create', methods=['OPTIONS', 'POST'])
async def hist_create_shortcode(request):
    """create a hi.st shortcode
    input:
        url - url to shorten
        name - optional name to use as shortcode
    """
    print(request.json)
    url = request.json.get('url')
    name = request.json.get('name')
    shortlinkfor = request.json.get('shortlinkfor')

    #print("url: ", url)

    if name:
        #print("hist create name ", name, " for ", url)
        reply = {'status': 'nok', 'msg': 'Create link by name not yet implemented'}

    else:
        #print("hi.st create link ", url)
        # get max num and create new link in one transaction
        # is probably thread- and lock safe
        r = g.run("""
            MATCH (m:Redirect)
            WITH max(m.num) AS next
            CREATE (r:Redirect) SET
                r.num = next+1,
                r.to = {url},
                r.created = timestamp(),
                r.last_redirect = 0,
                r.redirects = 0
            RETURN r.num as num
        """, url=url).evaluate()

        # shortcode = CONFIG.SHORT_BASE + tic.base66.encode(r)
        shortcode = "https://hi.st/{}".format(tic.base66.encode(r))

        if shortlinkfor:
            x = g.run("""
                MATCH (r:Redirect {num:{num}})
                MATCH (n {uuid:{node_uuid}})
                CREATE (r)-[x:SHORTLINK_FOR]->(n)
                SET x.url = {shortcode}
                RETURN x
                """, num=r, node_uuid=shortlinkfor, shortcode=shortcode)

        reply = {'status': 'ok', 'shortUrl': shortcode}

    # print(reply)

    return json(reply)

# ------------------------------------------------------------------------

def run_server():
	print("-> Using SANIC ", sanic.__version__)
	app.run(host="0.0.0.0", port=9000, debug=True)

if __name__=='__main__':
	run_server()
