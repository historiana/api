print('__init tic')

from .tic import *
import uuid

# javascript compatible timestamp
NOW = lambda : int(time.time()*1000)

# create uuid's for nodes without plugin
NEW_UUID = lambda : str(uuid.uuid4())

from . import base66
