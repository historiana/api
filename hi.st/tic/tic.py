
import datetime, time
from graph import g

def log_event(member_uuid, msg):

    r = g.run("""
        MATCH (m:Member {uuid:{member_uuid}}) 
        CREATE (e:EventLog {msg: {msg}, ts: timestamp()})<-[:_EVENT]-(m)
        RETURN null""", {'member_uuid': member_uuid, 'msg': msg}).evaluate()
    return r


def js_now():
    d = datetime.datetime.utcnow()
    return int(time.mktime(d.timetuple())) * 1000

def get_remote_addr(request):
    # get the remote addr for this request; only interested in the IP# part
    # not the socket; should Sanic store it in a list instead?

    try:
        ip = request.ip[0]
    except:
        ip = None

    if not ip:
        try:
            ra = request.headers.get('Remote-Addr')
            ra = ra.split(':')[0]
        except:
            pass

    return ip


def log(msg):
    print(js_now(), msg)