Queries uit te voeren:

// oude redirect records zonder created veld weggooien
MATCH (n:Redirect) where not exists(n.created) delete n

# cleanups gedaan op newlive 26.6.2020

# deze en varianten om ongewenste entries te verwijderen

match (a:ContentItem {title:'The theatres of war'})-[r*1..3]-(b:ContentItem {title:'How the War was Fought'}) unwind r as x delete x

match (c:ContentItem) where c.embed_code contains "http:/" set c.embed_code=replace(c.embed_code, 'http:/', 'https:/')
