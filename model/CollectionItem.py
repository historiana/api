"""
van de excel:

title
introduction
url
type
period_start
period_end
origin
country
language
local_id
copyright
tags

"""


["title", "url", "description", "type", "filename", "order", "uuid"]


class CollectionItem:

    """A CollectionItem is a single item in a Collection, mostly an image"""

    props = {
        "title":
        "introduction":
        "url":
        "type":
        "period_start":
        "period_end":
        "origin":
        "country":
        "language":
        "local_id":

    }

    relations = {
        "copyright": { "target": "License", "name": "HAS_LICENSE"}
        "tags": { "target": "MyKeywords", "name": "MYTAG"}
    }

