from pptx import Presentation
from pptx.parts.image import ImagePart

from pptx.shapes.picture import Picture
from pptx.shapes.placeholder import PlaceholderPicture

import os
import os.path
import tempfile

from graph import g
from tic import NEW_UUID, api_response, status
from slugify import slugify

import json

def pptx_to_collection(filename, url):

  # process PPTX into array of images and texts
  data = analyze_pptx(filename, url)

  # check if the title exists; add a (n) for the current one
  titles = g.run("MATCH (c:Collection) where c.name contains {title} RETURN count(c)", data).evaluate()
  if titles>0:
    data['title'] = data['title']+" ({})".format(titles+1)
    data['slug'] = slugify(data['title'])

  g.run("""
    CREATE (c:Collection)
    SET 
      c.uuid = {uuid},
      c.pptx = {pptx},
      c.name = {title},
      c.slug = {slug},
      c.subtitle = {subtitle},
      c.description = {description},
      c.status  = 'uploaded',
      c.ts = timestamp()

    RETURN c
  """, data)

  for slide in data['slides']:

    print("silde: ", slide)
    slide['collection'] = data['uuid']
    print("EKYS: ", slide.keys())

    # if a slide has no image there is no need to store it as a CollectionItem
    # as these are all based around an image and some text
    if (slide.get('url')):
      g.run("""
        MATCH (c:Collection {uuid:{collection}})
        CREATE (ci:CollectionItem)-[:IN_COLLECTION]->(c)
        SET
          ci.uuid = apoc.create.uuid(),
          ci.order = {slide},
          ci.title = {title},
          ci.filename = {filename},
          ci.url = {url},
          ci.description = {text}
        RETURN ci

      """, slide)




  return api_response(status.OK)
  

#============================================================================
def analyze_pptx(filename, urlpath):

  pptx_dir = os.path.dirname(filename)
  pptx = os.path.basename(filename)
  p = Presentation(filename)
  slides = []

  #tmpdir = tempfile.mkdtemp()
  #os.chmod(tmpdir, 0o777) #open for webserving
  try:
    images_dir = pptx_dir+'/images'
    os.mkdir(images_dir, 0o777)
  except:
    print("{} ALREADY EXISTS or PERMISSION PROBLEM".format(images_dir))
    pass

  title = ''
  subtitle = ''
  description = ''

  for cnt, slide in enumerate(p.slides):

    rec = dict(
      text = [],
      slide = cnt,
      filename = None,
      url = None
    )

    for (o,s) in enumerate(slide.shapes):
      print("process slide ")
      try: 
        rec['text'].append(s.text)
      except: 
        pass
              
      if type(s) in [Picture,PlaceholderPicture]:
        fname = "{:03d}_{:03d}_{}".format(cnt, o, s.image.filename)
        fname = fname.replace("jpeg","jpg")
        print("create file: ", fname)
        f = open("%s/%s" % (images_dir, fname), "wb")
        f.write(s.image.blob)
        f.close()
        rec['filename'] = fname
        rec['url'] = "/".join([urlpath, 'images', fname])
        # rec['image'] = { 
        #   'filename':fname,
        #   'url': "{}/{}".format(images_dir, fname),
        #   'content_type':s.image.content_type, 
        #   'sha1': s.image.sha1 
        # }
        #print("rec: ", json.dumps(rec, indent=4))

    try:
      rec['title'] = rec['text'][0]
      rec['text'] = rec['text'][1:]
      if cnt==0:
        try:
          (title,subtitle) = rec['title'].split('\n', 1)
          description = "\n".join(rec['text'])
        except ValueError:
          title = rec['title']
          subtitle = ''


    except:
      rec['title'] = 'No title found'
    

    slides.append(rec)
  # basename because we want a asset path

  return dict(
    uuid = NEW_UUID(),
    pptx = pptx,
    title = title,
    description = description,
    slug = slugify(title),
    subtitle = subtitle,
    path = os.path.basename(pptx_dir)+'/images', 
    slides = slides
  )

  
if __name__ == "__main__":
  #pptx = "/Users/paulj/Projects/Historiana/development/server/powerpoint/Internment the Bigger Picture in format.pptx"
  pptx = "./Posters of Communist China.pptx"
  data = analyze_pptx(pptx)
  print("data: ", json.dumps(data, indent=4))


