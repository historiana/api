from pptx import Presentation
from pptx.parts.image import ImagePart

from pptx.shapes.picture import Picture
from pptx.shapes.placeholder import PlaceholderPicture

import os
import os.path
import tempfile

import json

#============================================================================
def analyze_pptx(filename):

  pptx_dir = os.path.dirname(filename)
  print("dir: ", pptx_dir)
  p = Presentation(filename)
  slides = []

  #tmpdir = tempfile.mkdtemp()
  #os.chmod(tmpdir, 0o777) #open for webserving
  try:
    images_dir = pptx_dir+'/images'
    os.mkdir(images_dir, 0o777)
  except:
    print("{} ALREADY EXISTS or PERMISSION PROBLEM".format(images_dir))
    pass


  for cnt, slide in enumerate(p.slides):

    rec = {}
    rec['text'] = []
    rec['slide'] = cnt

    for (o,s) in enumerate(slide.shapes):
      #print "shape: ", s

      try: 
        rec['text'].append(s.text)
      except: 
        pass
              
      if type(s) in [Picture,PlaceholderPicture]:
        fname = "{:03d}_{:03d}_{}".format(cnt, o, s.image.filename)
        fname = fname.replace("jpeg","jpg")
        print("create file: ", fname)
        f = open("%s/%s" % (images_dir, fname), "wb")
        f.write(s.image.blob)
        f.close()
        rec['image'] = { 
          'filename':fname,
          'content_type':s.image.content_type, 
          'sha1': s.image.sha1 
        }


    try:
      rec['title'] = rec['text'][0]
      rec['text'] = rec['text'][1:]
    except:
      rec['title'] = 'No title found'
    

    slides.append(rec)
  # basename because we want a asset path
  return os.path.basename(pptx_dir)+'/images', slides

  
if __name__ == "__main__":
  #pptx = "/Users/paulj/Projects/Historiana/development/server/powerpoint/Internment the Bigger Picture in format.pptx"
  pptx = "./Posters of Communist China.pptx"
  data = analyze_pptx(pptx)
  print("data: ", json.dumps(data, indent=4))


