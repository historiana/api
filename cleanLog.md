# DELETE old stuff

match (a:App)-[r]-() detach delete a,r
match (a:AppState)-[r]-() detach delete a,r
match (a:AppState) delete a
match (w:WEG) delete w
match (w:Kip) delete w
match (w:MemberTransaction) delete w
match (n:Session) detach delete n
match (c:Collection) detach delete c
match (c:CollectionItem) detach delete c
match (c:ContentItems) detach delete c
match (l:LearningActivity) detach delete l
match (l:Link) detach delete l
match (l:LinkType) detach delete l

match (l:Module) detach delete l
match (l:ModuleItem) detach delete l
match (l:ModuleType) detach delete l
MATCH (n:ActivityDuration) detach delete n
MATCH (n:AdminIP) delete n
MATCH (n:Age) delete n
MATCH (n:Chapter) detach delete n
MATCH (n:DataProvider) delete n
MATCH (n:EventLog) detach delete n
MATCH (n:Invite) detach delete n
match (n:License) delete n
match (s:SearchPartner) detach delete s
match (s:Section) detach delete s
match (s:Status) detach delete s
MATCH (n:TeachingChallenges) delete n
MATCH (n:TeachingMethods) delete n
MATCH (n:Outcome) delete n

MATCH (n:HistoricalThinking) detach delete n




Constraints
drop constraint   ON ( collection:Collection ) ASSERT collection.uuid IS UNIQUE;
drop constraint   ON ( contentitem:ContentItem ) ASSERT contentitem.slug IS UNIQUE;
drop constraint   ON ( contentitem:ContentItem ) ASSERT contentitem.uuid IS UNIQUE;
drop constraint   ON ( contentprovider:ContentProvider ) ASSERT contentprovider.code IS UNIQUE;
drop constraint   ON ( copyright:Copyright ) ASSERT copyright.code IS UNIQUE;
drop constraint   ON ( event:Event ) ASSERT event.name IS UNIQUE;
drop constraint   ON ( group:Group ) ASSERT group.slug IS UNIQUE;
drop constraint   ON ( historicalthinking:HistoricalThinking ) ASSERT historicalthinking.slug IS UNIQUE;
drop constraint   ON ( language:Language ) ASSERT language.code IS UNIQUE;
drop constraint   ON ( learningactivity:LearningActivity ) ASSERT learningactivity.slug IS UNIQUE;
drop constraint   ON ( learningactivity:LearningActivity ) ASSERT learningactivity.uuid IS UNIQUE;
drop constraint   ON ( license:License ) ASSERT license.code IS UNIQUE;
drop constraint   ON ( link:Link ) ASSERT link.uuid IS UNIQUE;
drop constraint   ON ( linktype:LinkType ) ASSERT linktype.uuid IS UNIQUE;
drop constraint   ON ( location:Location ) ASSERT location.slug IS UNIQUE;
drop constraint   ON ( member:Member ) ASSERT member.uuid IS UNIQUE;
drop constraint   ON ( membertransaction:MemberTransaction ) ASSERT membertransaction.uuid IS UNIQUE;
drop constraint   ON ( module:Module ) ASSERT module.name IS UNIQUE;
drop constraint   ON ( module:Module ) ASSERT module.slug IS UNIQUE;
drop constraint   ON ( module:Module ) ASSERT module.uuid IS UNIQUE;
drop constraint   ON ( moduletype:ModuleType ) ASSERT moduletype.uuid IS UNIQUE;
drop constraint   ON ( period:Period ) ASSERT period.name IS UNIQUE;
drop constraint   ON ( person:Person ) ASSERT person.slug IS UNIQUE;
drop constraint   ON ( section:Section ) ASSERT section.title IS UNIQUE;
drop constraint   ON ( teachingchallenges:TeachingChallenges ) ASSERT teachingchallenges.slug IS UNIQUE;
drop constraint   ON ( teachingmethods:TeachingMethods ) ASSERT teachingmethods.slug IS UNIQUE;
drop constraint   ON ( theme:Theme ) ASSERT theme.slug IS UNIQUE;
drop constraint   ON ( tool:Tool ) ASSERT tool.code IS UNIQUE;


drop constraint  ON ( app:App ) ASSERT app.uuid IS UNIQUE;
drop constraint  ON ( chapter:Chapter ) ASSERT chapter.title IS UNIQUE;
drop constraint  ON ( collection:Collection ) ASSERT collection.name IS UNIQUE;
drop constraint  ON ( historicalthinking:HistoricalThinking ) ASSERT historicalthinking.slug IS UNIQUE;


















