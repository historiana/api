
from graph import g

data = g.run(
	"""
        MATCH (p:Partner)
        OPTIONAL MATCH (p)-[:LOGO]-(logo)
        OPTIONAL MATCH (p)-[:ICON]-(icon)
        WITH p,logo,icon
        ORDER BY p.name
        RETURN collect(p {
            .*,
            logo: { url: '/ua/'+logo.path, uuid: logo.uuid},
            icon: { url: '/ua/'+icon.url, uuid: icon.uuid}
        })

    """).evaluate()
