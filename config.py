"""
Historiana Global API configuration
"""

__version__ = (1, 0, 0)

import os

# print("config.py config for: {}".format(os.uname().nodename))
# print(os.uname().nodename)
# print('=========')


class Config:
    """
    Global configuration object
    from config import config
    HOME: used in /share-activity to create the correct link to a short url

    """

    CWD = os.getcwd()

    # NOTE: no trailing / in urls !
    CONFIGS = {
        # XXX url is op het moment de URL naar de assets in de API
        # betere naam verzinnen!
        "atom.webtic.net": {
            "comment": "local development",
            "url": "http://10.10.10.42:9000",
            "home": "http://localhost:8080",
            "shortener": "http://localhost:8080/hist",
            "development": True,
            "user_uploads": "/Users/paulj/Projects/Historiana/development/server/user_assets",
            "tasks": "/Users/paulj/Projects/Historiana/development/server/tasks",
            "mail_templates": "/Users/paulj/Projects/Historiana/development/server/email-templates",
        },
        "dev.historiana.eu": {
            "comment": "online dev server dev.historiana.eu",
            "url": "http://dev.historiana.eu",
            "home": "http://dev.historiana.eu",
            "shortener": "http://dev.hi.st",
            "rollbar": "beff09a0b213420fa5e3dbf58faba205",
            "ROLLBAR_ENV": "development",
            "utf8console": True,
            "user_uploads": "/web/sites/historiana.eu/site/user_assets",
            "tasks": "/web/sites/historiana.eu/api/tasks",
            "mail_templates": "/web/sites/historiana.eu/api/email-templates",
        },
        "live.historiana.eu": {
            "comment": "Production server",
            "url": "https://live.historiana.eu",
            "home": "https://live.historiana.eu",
            "shortener": "http://hi.st",
            "rollbar": "beff09a0b213420fa5e3dbf58faba205",
            "ROLLBAR_ENV": "production",
            "utf8console": True,
            "user_uploads": "/web/sites/historiana.eu/site/user_assets",
        },
        "hi.webtic.nl": {
            "url": "http://www.beta.historiana.eu",
            "home": "http://www.beta.historiana.eu",
            "shortener": "http://beta.hi.st",
            "user_uploads": os.getcwd() + "/user_assets",
        },
        "demo.historiana.eu": {
            "url": "http://demo.historiana.eu",
            "home": "http://demo.historiana.eu",
            "shortener": "http://demo.hi.st",
            "utf8console": True,
            "user_uploads": os.getcwd() + "/user_assets",
        },
    }

    CONFIGS["atom.local"] = CONFIGS["atom.webtic.net"]

    # setup some aliases for new partner VMs
    CONFIGS["poslovnik.dev.historiana.eu"] = CONFIGS["dev.historiana.eu"]

    # USER_UPLOADS = os.getcwd() + '/user_assets'
    USER_UPLOADS = CONFIGS.get(os.uname().nodename)["user_uploads"]
    USER_UPLOADS_URL = "/ua"
    SHORT_BASE = "https://hi.st/"
    SERVER = None

    # =========================================================
    def __init__(self):

        try:
            self.SERVER = os.uname().nodename
            self.SITE = self.CONFIGS.get(self.SERVER)
            self.SITE_URL = self.SITE["home"]
            self.development = self.SITE.get("development", False)
            self.opbeat = self.SITE.get("opbeat", False)
        except Exception as e:
            print("ABORT cannot get nodename")
            print("exception: ", e)

    # =========================================================
    def __repr__(self):
        return "{}".format(self.CONFIGS.get(self.SERVER))

    @property
    def HOME(self):
        return self.SITE["home"]


CONFIG = Config()
print("active config: ", CONFIG)
